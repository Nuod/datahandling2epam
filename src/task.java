import java.awt.geom.Arc2D;
import java.math.BigDecimal;

public class task {

    public BigDecimal AreaACircle(double r)
    {

        BigDecimal res= new BigDecimal(Math.PI * r * r);
        res.setScale(50);
        //System.out.println("The area of a circle" + res.toString());
    return res;
    }

    public Boolean ThreeIsSumm(String firstNumb, String secondNumb, String thirdNumb)
    {
        BigDecimal fNumb = new BigDecimal(Double.valueOf(firstNumb));
        fNumb.setScale(3, 4);
        BigDecimal sNumb = new BigDecimal(Double.valueOf(secondNumb));
        sNumb.setScale(3,4);
        BigDecimal tNumb = new BigDecimal(Double.valueOf(thirdNumb));
        tNumb.setScale(3,4);
        if (fNumb.add(sNumb).compareTo(tNumb) == 0){
            return true;
        }
        if (fNumb.add(tNumb).compareTo(sNumb) == 0){
            return true;
        }
        if (sNumb.add(tNumb).compareTo(fNumb) == 0){
        return true;
    }
    return false;

    }
    public void MaxAndMinOfthree(double fNumb, double sNumb, double tNumb)
    {
        double max, min;
        max = Double.max(fNumb,sNumb);
        max = Double.max(max,tNumb);
        min = Double.min(fNumb,sNumb);
        min = Double.min(min, tNumb);
        System.out.println("Max: " + max +"\nMin: " + min);
    }
}
